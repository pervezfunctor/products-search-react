
React = require 'react'
{ div, h3, input } = React.DOM;

class Counter extends React.Component
    constructor: (props) -> 
        super props
        @state = counter: 1
    handleUp: =>
        @setState counter: @state.counter + 1
    handleDown: =>
        @setState counter: Math.max 0, @state.counter - 1
    render: ->
        div null,
            h3 null, @state.counter
            input type: 'button', value: 'Up', onClick: @handleUp
            input type: 'button', value: 'Down', onClick: @handleDown

module.exports = Counter
