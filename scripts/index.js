import React from 'react';
import PagedSearchableProduct from './products';
import faker from 'faker';
import { List, Range } from "immutable";

var products = List(Range(1, 100).map(() => {
  return {
    name: faker.lorem.words(2).join(' '),
    price: faker.random.number(1000) };
}));

React.render(
	<PagedSearchableProduct products = { products } />,
	document.getElementById('root'));
