import React from 'react';
var { div, input, table, tr, th, td, thead, tbody } = React.DOM;

class ProductLine extends React.Component {
  render() {
    return (
        <tr>
          <td>{ this.props.product.name }</td>
          <td>{ this.props.product.price }</td>
        </tr>
    );
  }
}

class ProductsTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { productsPerPage: 8, currentPage: 0 };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({ currentPage: 0 });
  }
  totalPages() {
    return Math.ceil(
      this.props.products.size / this.state.productsPerPage);
  }
  handleNext() {
    const currentPage = Math.min(this.totalPages() - 1,
                               this.state.currentPage + 1);
    this.setState({ currentPage });
  }
  handlePrevious() {
    this.setState({ currentPage: Math.max(0, this.state.currentPage - 1) });
  }
  handleFirst() {
    this.setState({ currentPage: 0 });
  }
  handleLast() {
    this.setState({ currentPage: this.totalPages() - 1 });
  }
  handlePerPage(perPage) {
    perPage = parseInt(perPage, 10);
    if(!isNaN(perPage))
      this.setState({ productsPerPage: Math.max(1, perPage) });
  }
  render() {
    const products = this.props.products
          .skip(this.state.currentPage * this.state.productsPerPage)
          .take(this.state.productsPerPage)
          .toArray();
    return (
        <div className = 'display:table'>
          <table width = '100%'>
            <thead>
              <tr>
                <td>Name</td>
                <td>Price</td>
              </tr>
            </thead>
            <tbody>
              { products.map(p => <ProductLine product = { p } />) }
            </tbody>
          </table>
          <Pager
            pageSize = { this.state.productsPerPage }
            currentPage = { this.state.currentPage }
            totalPages = { this.totalPages() }
            handleNext = { this.handleNext.bind(this) }
            handlePrevious = { this.handlePrevious.bind(this) }
            handleFirst = { this.handleFirst.bind(this) }
            handleLast = { this.handleLast.bind(this) }
            handlePerPage = { this.handlePerPage.bind(this) } />
        </div>
    );
  }
}

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <div>
          <input
            type = 'text'
            ref = 'searchBox'
            onChange = { evt => this.props.onSearch(evt.target.value)   }
              />
        </div>
    );
  }
}

class Pager extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <div>
          <input
            type = 'button'
            value = '<<--'
            onClick = { this.props.handleFirst } />
          <input
            type = 'button'
            value = '<-'
            onClick = { this.props.handlePrevious } />
          <input
            type = 'button'
            value = '->'
            onClick = { this.props.handleNext } />
          <input
            type = 'button'
            value = '-->>'
            onClick = { this.props.handleLast } />
          <div>
            <div>current page: { this.props.currentPage + 1 }</div>
            <div>total pages: { this.props.totalPages }</div>
            <div>
              <input
              type = 'text'
              onChange = { evt => this.props.handlePerPage(evt.target.value) } />
            </div>
          </div>
        </div>
    );
  }
}

class PagedSearchableProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = { searchText: '' };
  }
  handleSearch(searchText) {
    this.setState({ searchText: searchText });
  }
  render() {
    const products = this.props.products
          .filter(p => p.name.indexOf(this.state.searchText) != -1);
    return (
        <div>
          <SearchBar onSearch = { this.handleSearch.bind(this) } />
          <ProductsTable products = { products } />
        </div>
    );
  }
}

module.exports = PagedSearchableProduct;
